<?php

use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
"   "Husa de protectie Tellur Glass print pentru Apple iPhone XR, Silk
"   https://s12emagst.akamaized.net/products/19006/19005127/images/res_5ed8686a57e4540dafc4ae44f902ca2e_450x450_5ur1.jpg
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
           'name' => 'Telefon mobil Samsung Galaxy S10, Dual SIM, 128GB, 8GB RAM, 4G, Green',
           'description' => 'Recreat complet pentru o experienta de vizionare neintrerupta. Fara margini care sa-ti distraga atentia. Ecranul elegant Infinity-O Display, decupat perfect cu laserul, cu securitate sporita si Dynamic AMOLED, este cel mai inovator ecran Galaxy de pana acum.  ',
           'photo' => 'https://s12emagst.akamaized.net/products/20114/20113791/images/res_c8f1276fa0f35692ff77ec22a6fd1e6c_200x200_7una.jpg',
           'price' => 2000
        ]);
 
        DB::table('products')->insert([
            'name' => 'Telefon mobil Apple iPhone 7, 32GB, Black',
            'description' => 'iPhone 7 imbunatateste substantial cele mai importante aspecte care definesc experienta iPhone. Iti ofera noi sisteme avansate pentru camere. Cele mai bune performante si cea mai extinsa autonomie din toate timpurile pentru iPhone. Difuzoare stereo captivante. Cea mai mare luminozitate si cea mai bogata gama cromatica pentru un afisaj de iPhone. Rezistenta la stropire si apa. ',
            'photo' => 'https://s12emagst.akamaized.net/products/4159/4158442/images/res_bbdfaa9537747e2cd6dd46a657f9107b_450x450_q4h3.jpg',
            'price' => 2000
        ]);
 
        DB::table('products')->insert([
            'name' => 'Telefon mobil Apple iPhone 11, 64GB, Black',
            'description' => 'Sistem nou cu doua camere. Baterie pentru toata ziua. Cea mai rezistenta sticla folosita vreodata intr-un smartphone. Cel mai rapid procesor Apple din toate timpurile. Design - Pentru o zi minunata',
            'photo' => 'https://s12emagst.akamaized.net/products/25344/25343941/images/res_99d57ec9e3d9bb8d3242f384288ce0a3_450x450_anbu.jpg',
            'price' => 7000
        ]);
 
        DB::table('products')->insert([
            'name' => 'Telefon mobil Samsung Galaxy A10, Dual Sim, 32GB, 4G, Black, Card 32GB inclus',
            'description' => 'Telefon mobil Samsung Galaxy A10, Dual Sim, 32 GB, Card 32 GB, 4G, Black Display Infinity-V | Camera Luminoasa | Baterie Durabila
Galaxy A10 este un telefon destinat celor ce le face placere sa vizioneze videoclipuri, sa joace jocuri video, sa interactioneze in retelele de socializare sau chiar sa faca poze. Fa cunostinta cu dispozitivul construit pentru bugetul tau, Galaxy A10.',
            'photo' => 'https://s12emagst.akamaized.net/products/22495/22494114/images/res_633bb203b8457fde5c5abbe69c68a589_450x450_shs6.jpg',
            'price' => 1300
        ]);
 
        DB::table('products')->insert([
            'name' => 'Husa de protectie din silicon transparenta 360 pentru Samsung Galaxy A10',
            'description' => 'Husa prezentata este ideala pentru a oferi telefonului dumneavoastra protectia necesara impotriva uzurii, zgarieturilor, prafului sau chiar a socurilor. Husa este realizata din silicon ultraslim, previne alunecarea accidentala a telefonului, este flexibila si rezistenta in timp.',
            'photo' => 'https://s12emagst.akamaized.net/products/23246/23245709/images/res_52eb1b34dcbdefe547ef1a58bc052484_450x450_glt1.jpg',
            'price' => 40
        ]);
 
        DB::table('products')->insert([
            'name' => 'Husa de protectie Tellur Glass print pentru Apple iPhone XR, Silk',
            'description' => 'The device is in good cosmetic condition and will show minor scratches and/or scuff marks.',
            'photo' => 'https://s12emagst.akamaized.net/products/19006/19005127/images/res_5ed8686a57e4540dafc4ae44f902ca2e_450x450_5ur1.jpg',
            'price' => 15
        ]);
    }
}
