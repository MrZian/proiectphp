<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ProductsController@index')->name('home');
 

Auth::routes();

Route::get('/home', 'ProductsController@index')->name('home');
 
Route::get('cart', 'ProductsController@cart')->middleware('auth');
 
Route::get('add-to-cart/{id}', 'ProductsController@addToCart')->middleware('auth');

Route::patch('update-cart', 'ProductsController@update');
 
Route::delete('remove-from-cart', 'ProductsController@remove');


